package d42.cavar.app_api.service.auto_complete

import com.google.gson.GsonBuilder
import d42.cavar.app_api.BuildConfig
import d42.cavar.app_api.constants.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

inline fun provideAuthInterceptor(): Interceptor {
    return Interceptor {
        val original = it.request()
        val originalHttpUrl = original.url()
        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(KEY, PLACE_AUTOCOMPLETE_KEY)
            .addQueryParameter(TYPES, REGIONS)
            .build()
        it.proceed(original.newBuilder().url(url).build())
    }
}

inline fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    if (BuildConfig.DEBUG) {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    } else {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
    }
    return httpLoggingInterceptor
}

inline fun provideGson() = GsonBuilder().create()

inline fun provideConverterFactory() = GsonConverterFactory.create(provideGson())

inline fun provideCallFactory() = RxJava2CallAdapterFactory.create()

inline fun createOkHttpClient() =
    OkHttpClient.Builder()
        .addInterceptor(provideAuthInterceptor())
        .addInterceptor(provideLoggingInterceptor())
        .build()

inline fun <reified T> createAutoCompleteWebService(): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(PLACE_AUTOCOMPLETE_BASE_URL)
        .client(createOkHttpClient())
        .addConverterFactory(provideConverterFactory())
        .addCallAdapterFactory(provideCallFactory())
        .build()
    return retrofit.create(T::class.java)
}
package d42.cavar.app_api.constants

import android.location.Location

//WEATHER REST
const val WEATHER_API_KEY = "2a8fc52212d1d020c4b3ac497469a6ef"
const val WEATHER_BASE_URL = "http://api.openweathermap.org/data/2.5/"
//AUTO COMPLETE
const val PLACE_AUTOCOMPLETE_BASE_URL = "https://maps.googleapis.com/maps/api/place/"
const val PLACE_AUTOCOMPLETE_KEY = "AIzaSyDvEkizmIsTbOdSklVl0pBFaCw7JRudfFg"

//Retrofit
const val APP_ID = "APPID"
const val REST_WEATHER = "REST_WEATHER"
const val REST_PLACE_AUTOCOMPLETE = "REST_PLACE_AUTOCOMPLETE"
const val UNITS = "units"
const val METRIC = "metric"
const val TYPES = "types"
const val REGIONS = "(regions)"
const val KEY = "key"
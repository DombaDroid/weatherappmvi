package d42.cavar.app_commons.utils

import android.text.format.DateFormat
import java.util.*

//Time
const val MILLIS = 1000
const val TIME_FORMAT = "HH:mm"

fun getDayName(time: Long): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time * MILLIS
    return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US)
}

fun getFormatedTime(time: Long): String {
    return DateFormat.format(TIME_FORMAT, time * MILLIS).toString()
}

fun getDayOfYear(time: Long): Int {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time * MILLIS
    return calendar.get(Calendar.DAY_OF_YEAR)
}

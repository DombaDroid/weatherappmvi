package d42.cavar.app_commons.views.view_holders

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import d42.cavar.app_commons.R
import d42.cavar.app_commons.constants.EMPTY_STRING

@EpoxyModelClass
abstract class SearchResultModel : EpoxyModelWithHolder<SearchResultViewHolder>() {

    @EpoxyAttribute lateinit var onClickListener: View.OnClickListener
    @EpoxyAttribute var name: String = EMPTY_STRING
    @EpoxyAttribute var payload: String = EMPTY_STRING
    @EpoxyAttribute var last: Boolean = false

    override fun getDefaultLayout(): Int = R.layout.cell_search_item

    override fun bind(holder: SearchResultViewHolder) {
        holder.tvSearch.text = name
        holder.tvSearch.setOnClickListener(onClickListener)
        if (last)
            holder.tvSearch.background = ContextCompat.getDrawable(holder.tvSearch.context, R.drawable.rectangle_white_bottom_corners)
        else
            holder.tvSearch.background = ContextCompat.getDrawable(holder.tvSearch.context, R.drawable.bottom_line_gray)

    }
}

class SearchResultViewHolder : EpoxyHolder(){
    lateinit var tvSearch: TextView
    override fun bindView(itemView: View) {
        tvSearch = itemView.findViewById(R.id.tvSearchItem)
    }
}
package d42.cavar.app_commons.utils

import java.net.InetAddress

fun isInternetAvailable(): Boolean {
    return try {
        val ipAddr = InetAddress.getByName("google.com")
        ipAddr.toString() != ""
    } catch (e: Exception) {
        false
    }
}
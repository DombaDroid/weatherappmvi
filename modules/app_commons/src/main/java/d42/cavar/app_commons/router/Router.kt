package d42.cavar.app_commons.router

import androidx.fragment.app.Fragment
import d42.cavar.api_common.models.WeatherCity

interface Router {
    fun createSplashFragment(): Fragment
    fun createHomeFragment(): Fragment
    fun createMapFragment(): Fragment
}
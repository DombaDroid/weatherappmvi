package d42.cavar.app_commons.extensions

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyRecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import d42.cavar.api_common.models.WeatherCity
import d42.cavar.app_commons.R
import d42.cavar.app_commons.constants.*
import kotlinx.android.synthetic.main.cell_network_error.view.*
import java.util.*

inline var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

inline var View.isInvisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

inline var View.isGone: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

inline var TextView.formatText
    get() = { text }.toString()
    set(value) {
        text = if (value.contains(INIT_INT.toString()))
            EMPTY_STRING
        else
            value
    }

fun ImageView.setImageSrc(id: Int) {
    if (id != INIT_INT)
        setImageResource(id)
}

fun CoordinatorLayout.setBackgroundSrc(context: Context, id: Int) {
    if (id != INIT_INT) {
        rootView.background = ContextCompat.getDrawable(context, id)
    }
}

fun String.capitaliseFirstLetter(): String {
    return if (isNotEmpty())
        substring(0, 1).toUpperCase() + substring(1)
    else
        EMPTY_STRING
}

fun InputMethodManager.hideKeyboard(activity: Activity) {
    val view = activity.currentFocus ?: View(activity)
    hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.showFakeSnackBar(string: Any, colorId: Int = R.color.gray_gun_metal) {
    val layout = LayoutInflater.from(context).inflate(R.layout.snack_layout, null)
    val title = layout.findViewById<TextView>(R.id.tvSnackText)
    val root = layout.findViewById<LinearLayout>(R.id.llSnackContainer)
    root.setBackgroundColor(ContextCompat.getColor(context!!, colorId))
    title.text = getString(string, context!!)
    with(Toast(context)) {
        setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        duration = Toast.LENGTH_SHORT
        view = layout
        show()
    }
}

fun FloatingActionButton.setVisible(isFabVisible: Boolean) {
    if (isFabVisible && !isVisible)
        show()
    else if (!isFabVisible && isVisible)
        hide()
}

fun EpoxyRecyclerView.withModels(buildModelsCallback: EpoxyController.() -> Unit) {
    setControllerAndBuildModels(object : EpoxyController() {
        override fun buildModels() {
            buildModelsCallback()
        }
    })
}

private fun getString(string: Any, context: Context): String {
    return when (string) {
        is String -> string
        is Int -> context.resources.getString(string)
        else -> EMPTY_STRING
    }
}

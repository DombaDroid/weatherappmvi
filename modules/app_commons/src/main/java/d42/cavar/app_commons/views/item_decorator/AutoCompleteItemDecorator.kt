package d42.cavar.app_commons.views.item_decorator

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import d42.cavar.app_commons.R

class AutoCompleteItemDecorator(val context: Context) : RecyclerView.ItemDecoration() {

    private val topMargin = context.resources.getDimensionPixelSize(R.dimen.dp18)
    private val padding = context.resources.getDimensionPixelSize(R.dimen.dp8)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) == 0) {
            view.setPadding(padding, topMargin, padding, padding)
        }
        super.getItemOffsets(outRect, view, parent, state)
    }
}
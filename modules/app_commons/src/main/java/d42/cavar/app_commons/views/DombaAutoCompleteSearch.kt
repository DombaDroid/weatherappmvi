package d42.cavar.app_commons.views

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.widget.RxTextView
import d42.cavar.api_common.models.Predictions
import d42.cavar.api_common.models.SearchItem
import d42.cavar.app_commons.R
import d42.cavar.app_commons.constants.INIT_INT
import d42.cavar.app_commons.extensions.withModels
import d42.cavar.app_commons.views.item_decorator.AutoCompleteItemDecorator
import d42.cavar.app_commons.views.view_holders.SearchResultModel_
import d42.cavar.app_commons.views.view_holders.searchResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.domba_auto_complite_cearch.view.*
import java.util.concurrent.TimeUnit

class DombaAutoCompleteSearch(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : FrameLayout(context, attrs, defStyleAttr), DombaAdapterListener.InnerAdapterListener, TextView.OnEditorActionListener,
    LifecycleObserver, View.OnClickListener, View.OnFocusChangeListener {
    constructor(context: Context?) : this(context, null, INIT_INT, INIT_INT)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, INIT_INT)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, INIT_INT)

    private var compositeDisposable = CompositeDisposable()
    private var listener: DombaAdapterListener.AdapterListener? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        if (compositeDisposable.isDisposed)
            compositeDisposable = CompositeDisposable()
        setupRxBindings()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() = compositeDisposable.clear()

    init {
        inflate(context, R.layout.domba_auto_complite_cearch, this)
        initViewActions()
    }

    private fun setupRxBindings() {
        compositeDisposable.addAll(
            RxTextView.textChangeEvents(etSearch)
                .debounce(400, TimeUnit.MILLISECONDS)
                .skip(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        if (it.text().toString().isEmpty())
                            epRecyclerView.clear()
                        else
                            listener?.wordForQuerry(it.text().toString())
                    },
                    onError = { it.printStackTrace() }
                ))
    }

    private fun initViewActions() {
        epRecyclerView.layoutManager = LinearLayoutManager(context)
        etSearch.setOnEditorActionListener(this)
        ivSearch.setOnClickListener(this)
        etSearch.onFocusChangeListener = this
    }

    fun addKeyWordListener(listener: DombaAdapterListener.AdapterListener) {
        this.listener = listener
    }

    fun setData(data: List<Predictions>) {
        val adapterList = data.map { SearchItem(it.description, it.city, false) }
        adapterList[adapterList.lastIndex].isLastItem = true
        epRecyclerView.withModels {
            for(i in data.indices){
                val prediction = data[i]
                searchResult{
                    id(prediction.description)
                    name(prediction.description)
                    payload(prediction.city)
                    last (i == data.size-1)
                    onClickListener { model, parentView, clickedView, position ->
                        listener?.onKeySelected(model.payload())
                    }
                }
            }
        }
    }

    override fun onClick(view: View) {
        view.requestFocusFromTouch()
        epRecyclerView.clear()
        listener?.onKeySelected(etSearch.text.toString())
    }

    override fun onItemClick(item: SearchItem) {
        compositeDisposable.dispose()
        etSearch.setText(item.name)
        setupRxBindings()
        epRecyclerView.clear()
        ivSearch.requestFocusFromTouch()
        listener?.onKeySelected(item.payload)
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) {
        if (!hasFocus)
            epRecyclerView.clear()
        else
            etSearch.setSelection(etSearch.text.toString().length)
    }

    override fun onEditorAction(view: TextView, actionId: Int, keyEvent: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            compositeDisposable.clear()
            listener?.onKeySelected(view.text.toString())
            setupRxBindings()
            return true
        }
        return false
    }

    fun linkLifecycle(fragment: Fragment) {
        fragment.lifecycle.addObserver(this)
    }

    fun clearList() {
        epRecyclerView.clear()
    }
}

interface DombaAdapterListener {

    interface InnerAdapterListener {
        fun onItemClick(item: SearchItem)
    }

    interface AdapterListener {
        fun wordForQuerry(word: String)
        fun onKeySelected(word: String)
    }
}
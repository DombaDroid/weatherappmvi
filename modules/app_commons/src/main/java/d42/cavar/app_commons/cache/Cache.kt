package d42.cavar.app_commons.cache

import d42.cavar.api_common.models.WeatherCity

class Cache {
    var weatherCity: WeatherCity? = null
    var lat: Double? = null
    var lng: Double? = null
}
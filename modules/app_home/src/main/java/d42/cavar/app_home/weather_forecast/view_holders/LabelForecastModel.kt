package d42.cavar.app_home.weather_forecast.view_holders

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.*
import d42.cavar.app_commons.constants.EMPTY_STRING
import d42.cavar.app_home.R

@EpoxyModelClass
abstract class LabelForecastModel : EpoxyModelWithHolder<LabelForecastViewHolder>() {
    @EpoxyAttribute var label: String = EMPTY_STRING

    override fun bind(holder: LabelForecastViewHolder) {
        holder.tvLabel.text = label
    }

    override fun getDefaultLayout(): Int = R.layout.cell_label
}

class LabelForecastViewHolder : EpoxyHolder(){
    lateinit var tvLabel : TextView
    override fun bindView(itemView: View) {
        tvLabel = itemView.findViewById(R.id.tvLabel)
    }
}
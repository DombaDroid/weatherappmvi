package d42.cavar.app_home.domain

import d42.cavar.api_common.interactors.auto_complete.AutoCompleteInteractor
import d42.cavar.api_common.interactors.weather.WeatherInteractor
import d42.cavar.api_common.models.WeatherCity
import d42.cavar.app_commons.cache.Cache
import io.reactivex.Observable

class HomeContext constructor(private val getWeatherInteractor: WeatherInteractor, private val getAutoCompleteInteractor: AutoCompleteInteractor, val cache: Cache) {

    fun getCurrentWeatherByCityName(name: String): Observable<WeatherCity> {
        return getWeatherInteractor.getCurrentWeatherByCityName(name)
    }

    fun getCurrentWeatherByLatLng(): Observable<WeatherCity> {
        return getWeatherInteractor.getCurrentWeatherByCoordinates(cache.lat!!, cache.lng!!)
    }

    fun getWeatherCityCache() = cache.weatherCity!!

    fun cacheWeatherCity(weatherCity: WeatherCity) {
        cache.weatherCity = weatherCity
    }

    fun getAutoCompletePredictionList(word: String) = getAutoCompleteInteractor.getAutoCompleteList(word)

    fun removeLatLngCache() {
        cache.lat = null
        cache.lng = null
    }
}
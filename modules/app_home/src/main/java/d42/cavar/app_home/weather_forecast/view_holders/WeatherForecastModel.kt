package d42.cavar.app_home.weather_forecast.view_holders

import android.content.res.Resources
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.*
import d42.cavar.app_home.R
import d42.cavar.app_commons.constants.EMPTY_STRING
import d42.cavar.app_commons.constants.INIT_INT
import d42.cavar.app_commons.extensions.setImageSrc
import kotlinx.android.synthetic.main.cell_weather_forecaste_item.view.*

@EpoxyModelClass()
abstract class WeatherForecastModel : EpoxyModelWithHolder<WeatherForecastViewHolder>() {
    @EpoxyAttribute
    var iconId: Int = INIT_INT
    @EpoxyAttribute
    var time: String = EMPTY_STRING
    @EpoxyAttribute
    var description: String = EMPTY_STRING
    @EpoxyAttribute
    var temp: Int = INIT_INT
    private lateinit var resources: Resources

    override fun bind(holder: WeatherForecastViewHolder) {
        resources = holder.ivIcon.resources
        holder.ivIcon.setImageSrc(iconId)
        holder.tvTime.text = time
        holder.tvDescription.text = description
        holder.tvTemp.text = resources.getString(R.string.temp_degree, temp)
    }

    override fun getDefaultLayout(): Int = R.layout.cell_weather_forecaste_item
}

public class WeatherForecastViewHolder : EpoxyHolder() {
    lateinit var ivIcon: ImageView
    lateinit var tvTime: TextView
    lateinit var tvDescription: TextView
    lateinit var tvTemp: TextView

    override fun bindView(itemView: View) {
        ivIcon = itemView.findViewById(R.id.ivIcon)
        tvTime = itemView.findViewById(R.id.tvTime)
        tvDescription = itemView.findViewById(R.id.tvDescription)
        tvTemp = itemView.findViewById(R.id.tvTemp)
    }
}


package d42.cavar.app_home.weather_forecast.ui

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import d42.cavar.app_commons.constants.INIT_INT
import d42.cavar.app_commons.extensions.capitaliseFirstLetter
import d42.cavar.app_commons.extensions.withModels
import d42.cavar.app_commons.utils.getDayName
import d42.cavar.app_commons.utils.getDayOfYear
import d42.cavar.app_commons.utils.getFormatedTime
import d42.cavar.app_home.R
import d42.cavar.app_home.utils.prepareBackgroundImage
import d42.cavar.app_home.utils.prepareIcon
import d42.cavar.app_home.weather_forecast.view_holders.header
import d42.cavar.app_home.weather_forecast.view_holders.labelForecast
import d42.cavar.app_home.weather_forecast.view_holders.weatherForecast
import d42.cavar.papercut.mvi.base.MviBottomSheet
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.bottom_sheet_weather_forecast.*
import java.util.ArrayList

class WeatherForecastBottomSheet :
    MviBottomSheet<WeatherForecastViewModel, WeatherForecastViewModel.ViewState, WeatherForecastViewModel.Intent, WeatherForecastViewModel.Event>(
        WeatherForecastViewModel::class
    ) {

    companion object {
        const val TAG = "WeatherForecastBottomSheet"

        fun newInstance(): BottomSheetDialogFragment = WeatherForecastBottomSheet()
    }

    private val intentSubject = PublishSubject.create<WeatherForecastViewModel.Intent>()

    override val layoutRes = R.layout.bottom_sheet_weather_forecast

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        epRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun createIntents(): Observable<WeatherForecastViewModel.Intent> {
        return intentSubject
    }

    override fun render(viewState: WeatherForecastViewModel.ViewState) {
        var previousType = INIT_INT
        epRecyclerView?.withModels {
            header {
                id(-1)
                onClickListener { _, _, _, _ ->
                    intentSubject.onNext(WeatherForecastViewModel.Intent.OnDismiss)
                }
            }
            for (item in viewState.list) {
                if (getDayOfYear(item.time) != previousType) {
                    previousType = getDayOfYear(item.time)
                    labelForecast {
                        id(-item.time)
                        label(getDayName(item.time))
                    }
                }
                weatherForecast {
                    id(item.time)
                    iconId(prepareIcon(item.icon))
                    time(getFormatedTime(item.time))
                    description(item.weatherInfo.capitaliseFirstLetter())
                    temp(Math.round(item.temp))
                }
            }
        }
    }

    override fun onEvent(event: WeatherForecastViewModel.Event) {
        when(event){
            WeatherForecastViewModel.Event.Dismiss -> dismiss()
        }
    }
}
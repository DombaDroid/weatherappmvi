package d42.cavar.app_home.weather_forecast.view_holders

import android.view.View
import android.widget.ImageView
import com.airbnb.epoxy.*
import d42.cavar.app_home.R

@EpoxyModelClass
abstract class HeaderModel : EpoxyModelWithHolder<HeaderViewHolder>() {
    @EpoxyAttribute lateinit var onClickListener: View.OnClickListener

    override fun getDefaultLayout(): Int = R.layout.cell_header

    override fun bind(holder: HeaderViewHolder) {
        holder.ivDismiss.setOnClickListener(onClickListener)
    }
}

class HeaderViewHolder : EpoxyHolder(){
    lateinit var ivDismiss: ImageView
    override fun bindView(itemView: View) {
        ivDismiss = itemView.findViewById(R.id.ivDismiss)
    }
}
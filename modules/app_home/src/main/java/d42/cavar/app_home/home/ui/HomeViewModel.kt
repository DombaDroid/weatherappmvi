package d42.cavar.app_home.home.ui

import d42.cavar.api_common.models.Predictions
import d42.cavar.api_common.models.WeatherCity
import d42.cavar.app_commons.constants.EMPTY_STRING
import d42.cavar.app_commons.extensions.capitaliseFirstLetter
import d42.cavar.app_commons.utils.isInternetAvailable
import d42.cavar.app_home.R
import d42.cavar.app_home.domain.HomeContext
import d42.cavar.app_home.utils.prepareBackgroundColor
import d42.cavar.app_home.utils.prepareBackgroundImage
import d42.cavar.papercut.intent
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.Callable

class HomeViewModel constructor(val homeContext: HomeContext) :
    MviViewModel<HomeViewModel.ViewState, HomeViewModel.Intent, HomeViewModel.Event>() {
    private val eventsSubject = PublishSubject.create<Event>()

    override val initialStateCallable = Callable {
        val item = homeContext.getWeatherCityCache()
        ViewState(
            isLoading = false,
            temp = Math.round(item.temp),
            tempMin = Math.round(item.tempMin),
            tempMax = Math.round(item.tempMax),
            humidity = item.humidity,
            wind = Math.round(item.wind),
            pressure = Math.round(item.pressure),
            weatherInfo = item.weatherInfo.capitaliseFirstLetter(),
            icon = item.weatherInfo,
            backgroundDrawableId = prepareBackgroundImage(item.icon),
            backgroundColorId = prepareBackgroundColor(item.icon),
            name = item.name ?: EMPTY_STRING,
            list = item.list
        )
    }

    override val reducer = { viewState: ViewState, intent: Intent ->
        when (intent) {
            is Intent.RenderHome -> {
                val item = intent.item
                viewState.copy(
                    isLoading = false,
                    temp = Math.round(item.temp),
                    tempMin = Math.round(item.tempMin),
                    tempMax = Math.round(item.tempMax),
                    humidity = item.humidity,
                    wind = Math.round(item.wind),
                    pressure = Math.round(item.pressure),
                    weatherInfo = item.weatherInfo,
                    icon = item.weatherInfo,
                    backgroundDrawableId = prepareBackgroundImage(item.icon),
                    backgroundColorId = prepareBackgroundColor(item.icon),
                    name = item.name ?: EMPTY_STRING,
                    list = item.list
                )
            }
            is Intent.Loading -> viewState.copy(isLoading = intent.isLoading)
            else -> viewState
        }
    }

    override fun bindIntents(obs: Observable<Intent>): Observable<Intent> {
        return obs.publish { selector ->

            val checkNewLocation = selector.intent<Intent.CheckNewLocation>()
                .flatMap {
                    if (homeContext.cache.lat != null && homeContext.cache.lng != null) {
                        homeContext.getCurrentWeatherByLatLng()
                            .flatMap {
                                homeContext.removeLatLngCache()
                                homeContext.cacheWeatherCity(it)
                                Observable.just(it)
                            }
                            .map<Intent> { Intent.RenderHome(it) }
                            .startWith(Intent.Loading(true))
                            .doOnSubscribe { eventsSubject.onNext(Event.HideKeyboard) }
                            .onErrorReturn {
                                eventsSubject.onNext(
                                    Event.ShowSnackBar(
                                        R.string.something_gone_wrong,
                                        R.color.gray_gun_metal
                                    )
                                )
                                Intent.Loading(false)
                            }
                    } else {
                        Observable.just(Intent.NoOp).doOnNext { Intent.Loading(false) }
                    }
                }

            val refreshHome = selector.intent<Intent.ClickRefresh>()
                .flatMap {
                    homeContext.getCurrentWeatherByCityName(homeContext.getWeatherCityCache().name!!)
                        .map<Intent> { Intent.RenderHome(it) }
                        .onErrorReturn {
                            eventsSubject.onNext(Event.ShowSnackBar(R.string.refresh_error, R.color.gray_gun_metal))
                            Intent.Loading(false)
                        }
                        .startWith(Intent.Loading(true))
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                }

            val bottomSheet = selector.intent<Intent.PrepareBottomSheet>()
                .flatMap {
                    Observable.just(true)
                        .doOnNext { eventsSubject.onNext(Event.ShowBottomSheet) }
                        .map<Intent> { Intent.NoOp }
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                }


            val prepareMapFragment = selector.intent<Intent.PrepareMapFragment>()
                .flatMap {
                    Observable.just(true)
                        .doOnNext { eventsSubject.onNext(Event.ShowMapFragment) }
                        .map<Intent> { Intent.NoOp }
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                }

            val setUpAutoSearch = selector.intent<Intent.StartFragment>()
                .doOnNext { eventsSubject.onNext(Event.SetUpAutoSearch) }

            val autoComplete = selector.intent<Intent.AutoComplete>()
                .flatMap {
                    homeContext.getAutoCompletePredictionList(it.word)
                        .doOnNext { eventsSubject.onNext(Event.ShowAutoCompleteList(it)) }
                        .map<Intent> { Intent.NoOp }
                        .doOnError { it.stackTrace }
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                }

            val backgroundClick = selector.intent<Intent.BackgroundClick>()
                .flatMap {
                    Observable.just(true)
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                        .map<Intent> { Intent.NoOp }
                }

            val getWeatherCity = selector.intent<Intent.CitySelected>()
                .flatMap {
                    homeContext.getCurrentWeatherByCityName(it.word)
                        .doOnNext { homeContext.cache.weatherCity = it }
                        .map<Intent> { Intent.RenderHome(it) }
                        .onErrorReturn {
                            if (isInternetAvailable()) eventsSubject.doOnNext {
                                Event.ShowSnackBar(
                                    R.string.network_error_description,
                                    R.color.gray_gun_metal
                                )
                            } else eventsSubject.doOnNext {
                                Event.ShowSnackBar(
                                    R.string.server_error_description,
                                    R.color.gray_gun_metal
                                )
                            }
                            Intent.Loading(false)
                        }
                        .startWith(Intent.Loading(true))
                        .doOnSubscribe {
                            eventsSubject.onNext(Event.HideKeyboard)
                            eventsSubject.onNext(Event.HideAutoCompleteList)
                        }
                }

            return@publish Observable.merge(
                Arrays.asList(
                    refreshHome,
                    bottomSheet,
                    prepareMapFragment,
                    checkNewLocation,
                    setUpAutoSearch,
                    autoComplete,
                    getWeatherCity,
                    backgroundClick
                )
            )
        }.filter { it != Intent.NoOp }
    }

    override fun bindEvents(): Observable<Event> {
        return eventsSubject.hide()
    }

    data class ViewState(
        val isLoading: Boolean,
        val temp: Int,
        val tempMin: Int,
        val tempMax: Int,
        val humidity: Int,
        val wind: Int,
        val pressure: Int,
        val weatherInfo: String,
        val icon: String,
        val backgroundDrawableId: Int,
        val backgroundColorId: Int,
        val name: String,
        var list: List<WeatherCity>?
    )

    sealed class Intent {
        object NoOp : Intent()
        object PrepareBottomSheet : Intent()
        object PrepareMapFragment : Intent()
        object CheckNewLocation : Intent()
        object StartFragment : Intent()
        object BackgroundClick : Intent()
        data class AutoComplete(val word: String) : Intent()
        data class Loading(val isLoading: Boolean) : Intent()
        object ClickRefresh : Intent()
        data class RenderHome(val item: WeatherCity) : Intent()
        data class CitySelected(val word: String) : Intent()
    }

    sealed class Event {
        object ShowBottomSheet : Event()
        object ShowMapFragment : Event()
        object SetUpAutoSearch : Event()
        object HideKeyboard : Event()
        object HideAutoCompleteList : Event()
        data class ShowAutoCompleteList(val list: List<Predictions>) : Event() //This can be ViewState but custom view handle the list (clear and removal on focus)
        data class ShowSnackBar(val text: Int, val color: Int) : Event()
    }
}
package d42.cavar.app_home.home.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.transition.Slide
import com.jakewharton.rxbinding2.view.clicks
import d42.cavar.app_commons.extensions.*
import d42.cavar.app_commons.router.Router
import d42.cavar.app_commons.views.DombaAdapterListener
import d42.cavar.app_home.R
import d42.cavar.app_home.weather_forecast.ui.WeatherForecastBottomSheet
import d42.cavar.app_home.weather_forecast.ui.WeatherForecastBottomSheet.Companion.TAG
import d42.cavar.papercut.mvi.base.MviFragment
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import java.util.*

class HomeFragment :
    MviFragment<HomeViewModel, HomeViewModel.ViewState, HomeViewModel.Intent, HomeViewModel.Event>(HomeViewModel::class),
    DombaAdapterListener.AdapterListener {
    companion object {
        const val REQUEST_CODE = 10041941
        fun newInstance(): Fragment {
            return HomeFragment()
        }
    }

    private val router: Router by inject()
    private val inputMethodManager by inject<InputMethodManager>()
    private val intentSubject = PublishSubject.create<HomeViewModel.Intent>()

    override val layoutRes: Int = R.layout.fragment_home

    override fun createIntents(): Observable<HomeViewModel.Intent> {
        return Observable.merge(
            Arrays.asList(intentSubject,
                tvCity.clicks().map { HomeViewModel.Intent.ClickRefresh },
                tvWeatherForecast.clicks().map { HomeViewModel.Intent.PrepareBottomSheet },
                ivGps.clicks().map { HomeViewModel.Intent.PrepareMapFragment },
                clRoot.clicks().map { HomeViewModel.Intent.BackgroundClick })
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intentSubject.onNext(HomeViewModel.Intent.StartFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            intentSubject.onNext(HomeViewModel.Intent.CheckNewLocation)
        }
    }

    override fun render(viewState: HomeViewModel.ViewState) {
        view?.findViewById<RelativeLayout>(R.id.loader)?.isVisible = viewState.isLoading
        tvCurrentTemp.formatText = getString(R.string.temp_degree, viewState.temp)
        tvCurrentStatus.formatText = viewState.weatherInfo
        tvCity.formatText = viewState.name
        tvLow.formatText = getString(R.string.temp_degree, viewState.tempMin)
        tvHigh.formatText = getString(R.string.temp_degree, viewState.tempMax)
        tvHumidity.formatText = getString(R.string.temp_percentage, viewState.humidity)
        tvWindSpeed.formatText = getString(R.string.temp_kmh, viewState.wind)
        tvPressure.formatText = getString(R.string.temp_pressure, viewState.pressure)
        ivWeather.setImageSrc(viewState.backgroundDrawableId)
        clRoot.setBackgroundSrc(context!!, viewState.backgroundColorId)
    }

    override fun onEvent(event: HomeViewModel.Event) {
        when (event) {
            is HomeViewModel.Event.ShowSnackBar -> showFakeSnackBar(event.text, event.color)
            HomeViewModel.Event.ShowBottomSheet -> showBottomSheet()
            HomeViewModel.Event.ShowMapFragment -> showMapFragment()
            HomeViewModel.Event.SetUpAutoSearch -> {
                dombaAutoComplete.linkLifecycle(this)
                dombaAutoComplete.addKeyWordListener(this)
            }
            is HomeViewModel.Event.ShowAutoCompleteList -> dombaAutoComplete.setData(event.list)
            HomeViewModel.Event.HideKeyboard -> {
                inputMethodManager.hideKeyboard(activity!!)
                clRoot.requestFocusFromTouch()
            }
            HomeViewModel.Event.HideAutoCompleteList -> dombaAutoComplete.clearList()
        }
    }

    private fun showMapFragment() {
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        val fragment = router.createMapFragment()
        fragment.setTargetFragment(this, REQUEST_CODE)
        fragment.enterTransition = Slide(Gravity.END)
        fragmentTransaction?.add(android.R.id.content, fragment)
        fragmentTransaction?.addToBackStack(fragment.tag)
        fragmentTransaction?.commit()
    }

    private fun showBottomSheet() {
        val bottomSheet = WeatherForecastBottomSheet.newInstance()
        bottomSheet.show(childFragmentManager, TAG)
    }

    override fun onKeySelected(word: String) {
        intentSubject.onNext(HomeViewModel.Intent.CitySelected(word))
    }

    override fun wordForQuerry(word: String) {
        intentSubject.onNext(HomeViewModel.Intent.AutoComplete(word))
    }
}
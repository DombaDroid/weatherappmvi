package d42.cavar.app_home.weather_forecast.domain

import d42.cavar.app_commons.cache.Cache
import java.util.*

class WatherForecastContext constructor(val cache: Cache) {

    fun getWeatherCityCache() = cache.weatherCity!!
}
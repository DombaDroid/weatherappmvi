package d42.cavar.app_home.weather_forecast.ui

import d42.cavar.api_common.models.WeatherCity
import d42.cavar.app_home.weather_forecast.domain.WatherForecastContext
import d42.cavar.papercut.intent
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import d42.cavar.papercut.mvi.viewModel.Reducer
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.Callable

class WeatherForecastViewModel constructor(val watherForecastContext: WatherForecastContext) :
    MviViewModel<WeatherForecastViewModel.ViewState, WeatherForecastViewModel.Intent, WeatherForecastViewModel.Event>() {

    override val initialStateCallable: Callable<ViewState> = Callable {
        ViewState(list = watherForecastContext.getWeatherCityCache().list!!)
    }

    override val reducer = { viewState: ViewState, intent: Intent ->
        viewState
    }

    override fun bindIntents(obs: Observable<Intent>): Observable<Intent> {
        return obs.publish { selector ->
            return@publish selector.intent<Intent.OnDismiss>()
                .doOnNext { eventSubject.onNext(Event.Dismiss) }
                .map<Intent> { Intent.NoOp }
        }.filter { it != Intent.NoOp }
    }

    override fun bindEvents(): Observable<Event> {
        return eventSubject.hide()
    }

    private val eventSubject = PublishSubject.create<Event>()

    data class ViewState(val list: List<WeatherCity>)

    sealed class Intent {
        object NoOp : Intent()
        object OnDismiss : Intent()
    }

    sealed class Event {
        object Dismiss : Event()
    }
}
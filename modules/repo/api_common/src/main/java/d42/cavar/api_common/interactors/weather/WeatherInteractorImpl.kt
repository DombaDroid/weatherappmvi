package d42.cavar.api_common.interactors.weather

import d42.cavar.api_common.response.CurrentWeatherEntity
import d42.cavar.api_common.response.WeatherForecastEntity
import d42.cavar.api_common.response.ZipWeatherWrapperEntity
import d42.cavar.api_common.WeatherRestInterface
import d42.cavar.api_common.mappers.toWeatherCity
import d42.cavar.api_common.models.WeatherCity
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class WeatherInteractorImpl constructor(val weatherRestInterface: WeatherRestInterface) : WeatherInteractor {
    override fun getCurrentWeatherByCoordinates(latitude: Double, longitude: Double): Observable<WeatherCity> {
        return weatherRestInterface.getCurrentWeatherByCoordinates(latitude, longitude).toObservable()
            .flatMap {
                Observable.zip(
                    Observable.just(it),
                    weatherRestInterface.getWeatherForecast(it.name!!).toObservable(),
                    BiFunction<CurrentWeatherEntity, WeatherForecastEntity, WeatherCity> { currentWeather, weatherForecast ->
                        currentWeather.toWeatherCity(weatherForecast)
                    })
            }.subscribeOn(Schedulers.io())
    }

    override fun getCurrentWeatherByCityName(city: String): Observable<WeatherCity> {
        return Observable.zip(weatherRestInterface.getCurrentWeatherByCityName(city).toObservable(),
            weatherRestInterface.getWeatherForecast(city).toObservable(),
            BiFunction<CurrentWeatherEntity, WeatherForecastEntity, WeatherCity> { currentWeather, weatherForecast ->
                currentWeather.toWeatherCity(weatherForecast)
            }).subscribeOn(Schedulers.io())
    }
}
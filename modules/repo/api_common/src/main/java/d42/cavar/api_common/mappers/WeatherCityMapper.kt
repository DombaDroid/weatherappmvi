package d42.cavar.api_common.mappers

import d42.cavar.api_common.models.WeatherCity
import d42.cavar.api_common.response.CurrentWeatherEntity
import d42.cavar.api_common.response.WeatherForecastEntity

fun CurrentWeatherEntity.toWeatherCity(weatherForecastEntity: WeatherForecastEntity) : WeatherCity {
    return WeatherCity(temp = mainInformation.temp,
        tempMin = mainInformation.minTemp,
        tempMax = mainInformation.maxTemp,
        time = time,
        humidity = mainInformation.humidity,
        wind = wind.speed,
        pressure = mainInformation.pressure,
        weatherInfo = weatherList[0].description,
        icon = weatherList[0].icon,
        name = name,
        list = weatherForecastEntity.list.map { it.toWeatherCity() })
}

fun CurrentWeatherEntity.toWeatherCity(): WeatherCity{
    return WeatherCity(
        temp = mainInformation.temp,
        tempMin = mainInformation.minTemp,
        tempMax = mainInformation.maxTemp,
        time = time,
        humidity = mainInformation.humidity,
        wind = wind.speed,
        pressure = mainInformation.pressure,
        weatherInfo = weatherList[0].description,
        icon = weatherList[0].icon,
        name = name,
        list = null
    )
}
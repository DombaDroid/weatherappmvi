package d42.cavar.api_common.models

import com.google.gson.annotations.SerializedName

data class Predictions(val description: String, val city: String)
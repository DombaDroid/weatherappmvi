package d42.cavar.api_common.response

import com.google.gson.annotations.SerializedName

data class AutoCompleteEntity (@SerializedName("predictions") val predictionsEntity: List<PredictionsEntity>,
                          @SerializedName("status") val status: String)
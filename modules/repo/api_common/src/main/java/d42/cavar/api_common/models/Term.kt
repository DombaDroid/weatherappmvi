package d42.cavar.api_common.models

import com.google.gson.annotations.SerializedName

data class Term (@SerializedName("value") val city: String)
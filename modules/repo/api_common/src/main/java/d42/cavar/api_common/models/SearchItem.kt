package d42.cavar.api_common.models

data class SearchItem(val name: String, val payload: String, var isLastItem: Boolean)
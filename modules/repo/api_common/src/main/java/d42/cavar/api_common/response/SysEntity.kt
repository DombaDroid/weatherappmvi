package d42.cavar.api_common.response

import com.google.gson.annotations.SerializedName

data class SysEntity(
    @SerializedName("sunrise") val sunrise: Long,
    @SerializedName("sunset") val sunset: Long
)
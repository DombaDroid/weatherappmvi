package d42.cavar.api_common.response

class ZipWeatherWrapperEntity (val weather: CurrentWeatherEntity, val weatherForecast: WeatherForecastEntity) {
}
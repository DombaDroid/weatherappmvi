package d42.cavar.api_common.response

import com.google.gson.annotations.SerializedName

data class MainInformationEntity(@SerializedName("temp") val temp: Float,
                           @SerializedName("humidity") val humidity: Int,
                           @SerializedName("pressure") val pressure: Float,
                           @SerializedName("temp_min") val minTemp: Float,
                           @SerializedName("temp_max") val maxTemp: Float)
package d42.cavar.api_common.interactors.auto_complete

import d42.cavar.api_common.AutoCompleteRestInterface
import d42.cavar.api_common.mappers.toPredictionList
import d42.cavar.api_common.models.Predictions
import d42.cavar.api_common.response.AutoCompleteEntity
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class AutoCompleteInteractorImpl constructor(val autoCompleteRestInterface: AutoCompleteRestInterface) : AutoCompleteInteractor {

    override fun getAutoCompleteList(word: String): Observable<List<Predictions>> {
        return autoCompleteRestInterface.getAutocompletedCities(word)
            .flatMapObservable { autoComplete: AutoCompleteEntity ->  Observable.just(autoComplete.toPredictionList())}
            .subscribeOn(Schedulers.io())
    }
}
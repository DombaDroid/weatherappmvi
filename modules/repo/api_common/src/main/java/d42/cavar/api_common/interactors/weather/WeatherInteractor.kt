package d42.cavar.api_common.interactors.weather

import d42.cavar.api_common.models.WeatherCity
import d42.cavar.api_common.response.WeatherForecastEntity
import d42.cavar.api_common.response.ZipWeatherWrapperEntity
import io.reactivex.Observable
import io.reactivex.Single

interface WeatherInteractor {
    fun getCurrentWeatherByCoordinates(latitude: Double, longitude: Double) : Observable<WeatherCity>
    fun getCurrentWeatherByCityName(city: String) : Observable<WeatherCity>
}
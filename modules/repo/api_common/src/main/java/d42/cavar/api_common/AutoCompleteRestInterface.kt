package d42.cavar.api_common

import d42.cavar.api_common.response.AutoCompleteEntity
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AutoCompleteRestInterface {
    @GET("autocomplete/json")
    fun getAutocompletedCities(@Query("input") word: String): Single<AutoCompleteEntity>
}
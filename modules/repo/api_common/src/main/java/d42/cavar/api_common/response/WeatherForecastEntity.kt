package d42.cavar.api_common.response

import com.google.gson.annotations.SerializedName

data class WeatherForecastEntity (@SerializedName("list") val list: List<CurrentWeatherEntity>)
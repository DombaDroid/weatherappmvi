package d42.cavar.api_common.mappers

import d42.cavar.api_common.models.Predictions
import d42.cavar.api_common.response.AutoCompleteEntity

fun AutoCompleteEntity.toPredictionList(): List<Predictions>{
    return predictionsEntity.map { Predictions(it.descriptionEntity, it.termsEntity[0].city) }
}
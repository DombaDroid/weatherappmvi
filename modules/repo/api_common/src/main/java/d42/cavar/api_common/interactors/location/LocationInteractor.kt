package d42.cavar.api_common.interactors.location

import android.location.Location
import io.reactivex.Observable

interface LocationInteractor {
    fun getLatestLocations(): Observable<Location>
}
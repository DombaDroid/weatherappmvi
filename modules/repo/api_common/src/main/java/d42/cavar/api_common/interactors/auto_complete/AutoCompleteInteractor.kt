package d42.cavar.api_common.interactors.auto_complete

import d42.cavar.api_common.models.Predictions
import io.reactivex.Observable

interface AutoCompleteInteractor {
    fun getAutoCompleteList(word: String) : Observable<List<Predictions>>
}
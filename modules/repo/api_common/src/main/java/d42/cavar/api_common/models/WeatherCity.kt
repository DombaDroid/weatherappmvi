package d42.cavar.api_common.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherCity(
    var temp: Float,
    var tempMin: Float,
    var tempMax: Float,
    var humidity: Int,
    var time: Long,
    var wind: Float,
    var pressure: Float,
    val weatherInfo: String,
    val icon: String,
    val name: String?,
    var list: List<WeatherCity>?
) : Parcelable
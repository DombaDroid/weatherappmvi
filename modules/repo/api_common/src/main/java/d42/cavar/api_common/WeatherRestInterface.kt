package d42.cavar.api_common

import d42.cavar.api_common.response.CurrentWeatherEntity
import d42.cavar.api_common.response.WeatherForecastEntity
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherRestInterface {
    @GET("weather")
    fun getCurrentWeatherByCoordinates(@Query("lat") latitude: Double, @Query("lon") longitude: Double): Single<CurrentWeatherEntity>

    @GET("weather")
    fun getCurrentWeatherByCityName(@Query("q") city: String): Single<CurrentWeatherEntity>

    @GET("forecast")
    fun getWeatherForecast(@Query("q") city: String): Single<WeatherForecastEntity>
}
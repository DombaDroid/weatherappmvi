package d42.cavar.api_common.constants

//PERMISSIONS
val REQUEST_FINE_LOCATION = 104

//DEFAULT VALUES
const val DEFAULT_CITY = "Osijek"

//Location
val DEFAULT_CITY_LATITUDE: Double = 45.553551
val DEFAULT_CITY_LONGITUDE: Double = 18.694536
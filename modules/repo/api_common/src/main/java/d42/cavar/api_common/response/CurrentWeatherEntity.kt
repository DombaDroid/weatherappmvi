package d42.cavar.api_common.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CurrentWeatherEntity(
    @SerializedName("sys") val sys: SysEntity,
    @SerializedName("dt") val time: Long,
    @SerializedName("weather") val weatherList: List<WeatherItemEntity>,
    @SerializedName("main") val mainInformation: MainInformationEntity,
    @SerializedName("wind") val wind: WindEntity,
    @SerializedName("name") val name: String?
)
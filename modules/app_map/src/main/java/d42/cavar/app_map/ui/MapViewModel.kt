package d42.cavar.app_map.ui

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import d42.cavar.app_map.R
import d42.cavar.app_map.domain.MapContext
import d42.cavar.papercut.intent
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.Callable

class MapViewModel constructor(private val mapContext: MapContext) :
    MviViewModel<MapViewModel.ViewState, MapViewModel.Intent, MapViewModel.Event>() {
    private val eventSubject = PublishSubject.create<Event>()

    override val initialStateCallable = Callable {
        ViewState(isFabVisible = false, enableLocation = false, latLng = null, marker = null)
    }
    override val reducer = { viewState: ViewState, intent: Intent ->
        when (intent) {
            Intent.PermissionSuccess -> viewState.copy(enableLocation = true)
            Intent.PermissionDenied -> viewState.copy(enableLocation = false)
            is Intent.OnMapClick -> viewState.copy(latLng = intent.latLng, isFabVisible = true)
            else -> viewState
        }
    }

    override fun bindIntents(obs: Observable<Intent>): Observable<Intent> {
        return obs.publish { selector ->

            val startMapObservable = selector.intent<Intent.StartMap>()
                .doOnNext { eventSubject.onNext(Event.RequestMap) }
                .map<Intent> { Intent.NoOp }

            val mapReadyObservable = selector.intent<Intent.MapReady>()
                .doOnNext { eventSubject.onNext(Event.CheckPermission) }
                .map<Intent> { Intent.NoOp }

            val permissionSuccessObservable = selector.intent<Intent.PermissionSuccess>()
                .flatMap {
                    mapContext.getLocationInteractor().take(1)
                        .doOnNext { eventSubject.onNext(Event.MoveCamera(LatLng(it.latitude, it.longitude))) }
                        .map<Intent> { Intent.PermissionSuccess }
                        .onErrorReturn {
                            Event.ShowSnackBar(R.string.something_gone_wrong, R.color.gray_gun_metal)
                            Intent.NoOp
                        }
                }

            val permissionDeniedObservable = selector.intent<Intent.PermissionDenied>()
                .doOnNext { eventSubject.onNext(Event.ShowSnackBar(R.string.gps_error, R.color.gray_gun_metal)) }

            val confirmClickObservable = selector.intent<Intent.ConfirmClick>()
                .doOnNext { eventSubject.onNext(Event.FinishFragment) }

            val onMapClickObservable = selector.intent<Intent.OnMapClick>()
                .flatMap {
                    mapContext.saveLatLng(it.latLng)
                    Observable.just(it).doOnNext { eventSubject.onNext(Event.MoveCamera(it.latLng)) }
                }

            return@publish Observable.merge(
                Arrays.asList(
                    startMapObservable, mapReadyObservable,
                    permissionSuccessObservable, permissionDeniedObservable,
                    onMapClickObservable, confirmClickObservable
                )
            )
        }.filter { it != Intent.NoOp }
    }

    override fun bindEvents(): Observable<Event> {
        return eventSubject.hide()
    }

    data class ViewState(
        val isFabVisible: Boolean,
        val enableLocation: Boolean,
        val latLng: LatLng?,
        var marker: Marker?
    )

    sealed class Intent {
        object NoOp : Intent()
        object MapReady : Intent()
        object StartMap : Intent()
        object PermissionSuccess : Intent()
        object PermissionDenied : Intent()
        object ConfirmClick : Intent()
        data class OnMapClick(val latLng: LatLng) : Intent()
    }

    sealed class Event {
        object CheckPermission : Event()
        object RequestMap : Event()
        object FinishFragment : Event()
        data class MoveCamera(val latLng: LatLng) : Event()
        data class ShowSnackBar(val text: Int, val color: Int) : Event()
    }
}
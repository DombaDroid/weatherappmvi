package d42.cavar.app_map.domain

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import d42.cavar.api_common.interactors.location.LocationInteractor
import d42.cavar.app_commons.cache.Cache
import io.reactivex.Observable

class MapContext(val cache: Cache,  private val getLocationInteractor: LocationInteractor) {
    fun saveLatLng(latLng: LatLng){
        cache.lat = latLng.latitude
        cache.lng = latLng.longitude
    }

    fun getLocationInteractor() : Observable<Location> {
        return getLocationInteractor.getLatestLocations()
    }
}
package d42.cavar.app_map.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.jakewharton.rxbinding2.view.clicks
import d42.cavar.api_common.constants.REQUEST_FINE_LOCATION
import d42.cavar.app_commons.extensions.setVisible
import d42.cavar.app_commons.extensions.showFakeSnackBar
import d42.cavar.app_map.R
import d42.cavar.papercut.mvi.base.MviFragment
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_map.*

class MapFragment :
    MviFragment<MapViewModel, MapViewModel.ViewState, MapViewModel.Intent, MapViewModel.Event>(MapViewModel::class),
    OnMapReadyCallback, GoogleMap.OnMapClickListener {

    companion object {
        const val TAG = "MapFragment"
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }

    private lateinit var mapFragment: SupportMapFragment
    private var googleMap: GoogleMap? = null
    private val intentSubject = PublishSubject.create<MapViewModel.Intent>()

    override val layoutRes = R.layout.fragment_map

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intentSubject.onNext(MapViewModel.Intent.StartMap)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        styleMap()
        googleMap.setOnMapClickListener(this)
        intentSubject.onNext(MapViewModel.Intent.MapReady)
    }

    override fun onMapClick(latLng: LatLng) {
        intentSubject.onNext(MapViewModel.Intent.OnMapClick(latLng))
    }

    override fun createIntents(): Observable<MapViewModel.Intent> {
        return Observable.merge(intentSubject, fab.clicks().map { MapViewModel.Intent.ConfirmClick })
    }

    @SuppressLint("MissingPermission")
    override fun render(viewState: MapViewModel.ViewState) {
        fab.setVisible(viewState.isFabVisible)
        googleMap?.isMyLocationEnabled = viewState.enableLocation
        if (viewState.marker == null)
            viewState.latLng?.let { viewState.marker = addMarker(it) }
        else
            viewState.marker?.position = viewState.latLng
    }

    override fun onEvent(event: MapViewModel.Event) {
        when (event) {
            MapViewModel.Event.RequestMap -> getAsyncMap()
            MapViewModel.Event.CheckPermission -> checkPermissions()
            is MapViewModel.Event.MoveCamera -> moveCamera(event.latLng)
            MapViewModel.Event.FinishFragment -> {
                targetFragment?.let {
                    it.onActivityResult(targetRequestCode, RESULT_OK, null)
                    fragmentManager?.popBackStack()
                }
            }
            is MapViewModel.Event.ShowSnackBar -> showFakeSnackBar(event.text, event.color)
        }
    }

    fun styleMap() {
        googleMap?.uiSettings?.isCompassEnabled = false
        val locationButton =
            (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                Integer.parseInt("2")
            )
        val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        rlp.setMargins(0, resources.getDimension(R.dimen.dp41).toInt(), resources.getDimension(R.dimen.dp16).toInt(), 0)
    }

    private fun moveCamera(latLng: LatLng) {
        googleMap?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun addMarker(latLng: LatLng): Marker? {
        return googleMap?.addMarker(
            MarkerOptions()
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin))
        )
    }

    private fun getAsyncMap() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            )
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_FINE_LOCATION)
            else
                intentSubject.onNext(MapViewModel.Intent.PermissionSuccess)
        } else {
            intentSubject.onNext(MapViewModel.Intent.PermissionSuccess)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_FINE_LOCATION && permissions.isNotEmpty())
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                intentSubject.onNext(MapViewModel.Intent.PermissionSuccess)
            else
                intentSubject.onNext(MapViewModel.Intent.PermissionDenied)
    }
}
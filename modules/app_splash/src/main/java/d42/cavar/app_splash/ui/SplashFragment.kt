package d42.cavar.app_splash.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.transition.Slide
import com.jakewharton.rxbinding2.view.clicks
import d42.cavar.api_common.constants.REQUEST_FINE_LOCATION
import d42.cavar.app_commons.extensions.isVisible
import d42.cavar.app_commons.extensions.showFakeSnackBar
import d42.cavar.app_commons.router.Router
import d42.cavar.app_splash.R
import d42.cavar.papercut.mvi.base.MviFragment
import d42.cavar.app_splash.ui.SplashViewModel.ViewState
import d42.cavar.app_splash.ui.SplashViewModel.Intent
import d42.cavar.app_splash.ui.SplashViewModel.Event
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_splash.*
import org.koin.android.ext.android.inject

class SplashFragment : MviFragment<SplashViewModel, ViewState, Intent, Event>(SplashViewModel::class) {

    companion object {
        const val TAG = "SplashFragment"
        fun newInstance(): SplashFragment {
            return SplashFragment()
        }
    }

    private val router: Router by inject()
    private val intentSubject = PublishSubject.create<Intent>()

    override val layoutRes = R.layout.fragment_splash

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intentSubject.onNext(Intent.StartSplash)
    }

    override fun createIntents(): Observable<Intent> {
        val retryClick = view!!.findViewById<TextView>(R.id.tvErrorTryAgain).clicks().map { Intent.StartSplash }
        return Observable.merge(retryClick, intentSubject)
    }

    override fun render(viewState: ViewState) {
        ivLoader.isVisible = viewState.isLoading
        view?.findViewById<RelativeLayout>(R.id.rlErrorRoot)?.isVisible = viewState.hasError
        viewState.errorImage?.let { view?.findViewById<ImageView>(R.id.ivErrorIcon)?.setImageResource(it) }
        view?.findViewById<TextView>(R.id.tvErrorTitle)?.text = getString(viewState.errorTitle)
        view?.findViewById<TextView>(R.id.tvErrorDescription)?.text = getString(viewState.errorDescription)
    }

    override fun onEvent(event: Event) {
        when (event) {
            Event.LoadHomeScreen -> showHomeFragment()
            Event.CheckPermission -> checkPermissions()
            is Event.ShowSnackBar -> showFakeSnackBar(event.text, event.color)
        }
    }

    private fun checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_FINE_LOCATION)
            else
                intentSubject.onNext(Intent.PermissionSuccess)
        } else {
            intentSubject.onNext(Intent.PermissionSuccess)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_FINE_LOCATION && permissions.isNotEmpty())
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                intentSubject.onNext(Intent.PermissionSuccess)
            else
                intentSubject.onNext(Intent.PermissionDenied)
    }

    private fun showHomeFragment() {
        fragmentManager?.let {

            val currentFragment = it.findFragmentById(android.R.id.content)
            val fragmentTransaction = it.beginTransaction()
            val fragment = router.createHomeFragment()
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, 0)
            fragmentTransaction.add(android.R.id.content, fragment)
            fragmentTransaction.commitNow()

            Handler().postDelayed({
                try {
                    val removeTransaction = it.beginTransaction()
                    removeTransaction.remove(currentFragment!!)
                    removeTransaction.commitAllowingStateLoss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }, 200)
        }
    }

}
package d42.cavar.app_splash.ui

import android.location.Location
import d42.cavar.api_common.constants.DEFAULT_CITY_LATITUDE
import d42.cavar.api_common.constants.DEFAULT_CITY_LONGITUDE
import d42.cavar.app_commons.cache.Cache
import d42.cavar.app_commons.constants.EMPTY_STRING
import d42.cavar.app_commons.utils.isInternetAvailable
import d42.cavar.app_splash.R
import d42.cavar.app_splash.domain.SplashContext
import d42.cavar.papercut.intent
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import d42.cavar.app_splash.ui.SplashViewModel.ViewState
import d42.cavar.app_splash.ui.SplashViewModel.Intent
import d42.cavar.app_splash.ui.SplashViewModel.Intent.StartSplash
import d42.cavar.app_splash.ui.SplashViewModel.Event
import d42.cavar.app_splash.ui.SplashViewModel.Intent.NoOp
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

class SplashViewModel constructor(val splashContext: SplashContext, val cache: Cache) :
    MviViewModel<ViewState, Intent, Event>() {
    private val eventsSubject = PublishSubject.create<Event>()

    override val initialStateCallable = Callable {
        ViewState(
            isLoading = true,
            hasError = false,
            errorImage = null,
            errorTitle = R.string.empty,
            errorDescription = R.string.empty
        )
    }

    override val reducer = { viewState: ViewState, intent: Intent ->
        when (intent) {
            is Intent.Loading -> viewState.copy(isLoading = true, hasError = false)
            is Intent.ServerError -> viewState.copy(
                isLoading = false,
                hasError = true,
                errorImage = R.drawable.ic_error_server,
                errorTitle = R.string.server_error,
                errorDescription = R.string.server_error_description
            )
            is Intent.NetworkError -> viewState.copy(
                isLoading = false,
                hasError = true,
                errorImage = R.drawable.ic_error_network,
                errorTitle = R.string.no_network_error,
                errorDescription = R.string.network_error_description
            )
            else -> viewState.copy(isLoading = false, hasError = false)
        }
    }

    override fun bindIntents(obs: Observable<Intent>): Observable<Intent> {
        return obs.publish { selector ->
            val startSplashObservable = selector.intent<StartSplash>()
                .flatMap {
                    Observable.just(it)
                        .map<Intent> { NoOp }
                        .doOnNext { eventsSubject.onNext(Event.CheckPermission) }
                        .startWith(Intent.Loading)
                }

            val currentLocationWeatherObservable = selector.intent<Intent.PermissionSuccess>()
                .flatMap {
                    splashContext.getLocationInteractor().take(1)
                        .timeout(5, TimeUnit.SECONDS, Observable.defer {
                            val defaultLocation = Location(EMPTY_STRING)
                            defaultLocation.latitude = DEFAULT_CITY_LATITUDE
                            defaultLocation.longitude = DEFAULT_CITY_LONGITUDE
                            Observable.just(defaultLocation)
                        }
                            .doOnNext {
                                eventsSubject.onNext(
                                    Event.ShowSnackBar(
                                        R.string.gps_error,
                                        R.color.gray_gun_metal
                                    )
                                )
                            })
                        .flatMap { splashContext.getCurrentWeatherByCoordinates(it.latitude, it.longitude) }
                        .doOnNext {
                            cache.weatherCity = it
                            eventsSubject.onNext(Event.LoadHomeScreen) }
                        .map<Intent> { NoOp }
                        .onErrorReturn { if (isInternetAvailable()) Intent.NetworkError else Intent.ServerError }
                        .startWith(Intent.Loading)
                }

            val defaultWeatherObservable = selector.intent<Intent.PermissionDenied>()
                .flatMap {
                    splashContext.getCurrentWeatherByCityName()
                        .doOnNext {
                            cache.weatherCity = it
                            eventsSubject.onNext(Event.LoadHomeScreen) }
                        .map<Intent> { NoOp }
                        .onErrorReturn { if (isInternetAvailable()) Intent.NetworkError else Intent.ServerError }
                        .startWith(Intent.Loading)
                }
            return@publish Observable.merge(
                startSplashObservable,
                currentLocationWeatherObservable,
                defaultWeatherObservable
            )
        }.filter { it != NoOp }
    }

    override fun bindEvents(): Observable<Event> {
        return eventsSubject.hide()
    }

    data class ViewState(
        val isLoading: Boolean,
        val hasError: Boolean,
        val errorImage: Int?,
        val errorTitle: Int,
        val errorDescription: Int
    )

    sealed class Intent {
        object StartSplash : Intent()
        object PermissionSuccess : Intent()
        object PermissionDenied : Intent()
        object Loading : Intent()
        object ServerError : Intent()
        object NetworkError : Intent()
        object NoOp : Intent()
    }

    sealed class Event {
        object CheckPermission : Event()
        object LoadHomeScreen : Event()
        data class ShowSnackBar(val text: Int, val color: Int) : Event()
    }
}
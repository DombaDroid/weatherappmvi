package d42.cavar.app_splash.domain

import android.location.Location
import d42.cavar.api_common.constants.DEFAULT_CITY
import d42.cavar.api_common.interactors.location.LocationInteractor
import d42.cavar.api_common.interactors.weather.WeatherInteractor
import d42.cavar.api_common.models.WeatherCity
import io.reactivex.Observable

class SplashContext constructor(private val getWeatherInteractor: WeatherInteractor, private val getLocationInteractor: LocationInteractor) {
    fun getCurrentWeatherByCityName() : Observable<WeatherCity> {
        return getWeatherInteractor.getCurrentWeatherByCityName(DEFAULT_CITY)
    }

    fun getCurrentWeatherByCoordinates(latitude: Double, longitude: Double) : Observable<WeatherCity>{
        return getWeatherInteractor.getCurrentWeatherByCoordinates(latitude, longitude)
    }

    fun getLocationInteractor() : Observable<Location>{
        return getLocationInteractor.getLatestLocations()
    }
}
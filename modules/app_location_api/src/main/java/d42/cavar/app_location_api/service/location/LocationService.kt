package d42.cavar.app_location_api.service.location

import android.content.Context
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.patloew.rxlocation.RxLocation
import d42.cavar.app_location_api.service.constants.LOCATION_UPDATE

fun provideLocationService(): LocationRequest {
    return LocationRequest.create().setPriority(PRIORITY_HIGH_ACCURACY).setInterval(LOCATION_UPDATE)
}

fun provideRxLocation(context: Context): RxLocation {
    return RxLocation(context)
}
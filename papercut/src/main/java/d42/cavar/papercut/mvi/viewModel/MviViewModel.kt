package d42.cavar.papercut.mvi.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import d42.cavar.papercut.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Callable

typealias Reducer<VS, I> = (VS, I) -> VS

abstract class MviViewModel<VS, I, E> : ViewModel() {
    abstract val initialStateCallable: Callable<VS>

    abstract val reducer: Reducer<VS, I>

    abstract fun bindIntents(obs: Observable<I>): Observable<I>
    abstract fun bindEvents(): Observable<E>

    private val _state = MutableLiveData<VS>()
    val state: LiveData<VS> = _state

    private val _events = SingleLiveEvent<E>()
    val events: LiveData<E> = _events

    private val disposables = CompositeDisposable()

    open fun bind(intents: Observable<I>, initialState: VS?) {
        disposables.add(bindEvents()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { event -> _events.value = event }
        )

        val startState = _state.value ?: initialState

        val boundIntents = bindIntents(intents)
            .observeOn(Schedulers.computation())

        val observable = if (startState != null) {
            boundIntents.scan(startState, BiFunction(reducer))
        } else {
            boundIntents.scanWith(initialStateCallable, BiFunction(reducer))
        }.distinctUntilChanged().observeOn(AndroidSchedulers.mainThread())

        disposables.add(observable.subscribe(
            { viewState -> _state.value = viewState },
            { error -> throw IllegalStateException("View state observable cannot error", error) },
            { throw IllegalStateException("View state observable cannot complete") }
        ))
    }

    fun unbind() {
        disposables.clear()
    }
}
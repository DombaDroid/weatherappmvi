package d42.cavar.papercut.mvi.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import org.koin.androidx.viewmodel.ext.android.viewModelByClass
import kotlin.reflect.KClass

abstract class MviActivity<VM : MviViewModel<VS, I, A>, VS, I, A>(
    clazz: KClass<VM>
) : AppCompatActivity(), MviView<VS, I, A> {

    private val myViewModel: VM by viewModelByClass(clazz)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewModel().apply {
            state.observe(this@MviActivity, Observer { viewState -> render(viewState!!) })
            events.observe(this@MviActivity, Observer { event -> onEvent(event!!) })
            bind(createIntents(), null)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        getViewModel().apply {
            unbind()
        }
    }

    private fun getViewModel(): VM {
        return myViewModel
    }
}
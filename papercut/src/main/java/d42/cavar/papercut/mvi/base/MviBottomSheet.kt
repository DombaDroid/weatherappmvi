package d42.cavar.papercut.mvi.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import org.koin.androidx.viewmodel.ext.android.viewModelByClass
import kotlin.reflect.KClass

abstract class MviBottomSheet<VM : MviViewModel<VS, I, A>, VS, I, A>(
    clazz: KClass<VM>
) : BottomSheetDialogFragment(), MviView<VS, I, A> {

    private val myViewModel: VM by viewModelByClass(clazz)
    protected abstract val layoutRes: Int

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                    savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewModel().apply {
            state.observe(this@MviBottomSheet, Observer { viewState -> render(viewState!!) })
            events.observe(this@MviBottomSheet, Observer { event -> onEvent(event!!) })
            bind(createIntents(), null)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getViewModel().apply {
            unbind()
        }
    }

    private fun getViewModel(): VM {
        return myViewModel
    }
}
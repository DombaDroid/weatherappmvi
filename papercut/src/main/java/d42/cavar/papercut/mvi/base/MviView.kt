package d42.cavar.papercut.mvi.base

import io.reactivex.Observable

internal interface MviView<VS, I, A> {

    fun createIntents(): Observable<I>

    fun render(viewState: VS)

    fun onEvent(event: A)

}
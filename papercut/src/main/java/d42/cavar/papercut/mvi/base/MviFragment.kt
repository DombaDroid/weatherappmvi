package d42.cavar.papercut.mvi.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import d42.cavar.papercut.mvi.viewModel.MviViewModel
import org.koin.androidx.viewmodel.ext.android.viewModelByClass
import kotlin.reflect.KClass

abstract class MviFragment<out VM : MviViewModel<VS, I, A>, VS, I, A>(
    clazz: KClass<VM>
) : Fragment(),
    MviView<VS, I, A> {

    private val myViewModel: VM by viewModelByClass(clazz)
    protected abstract val layoutRes: Int

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                    savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getViewModel().apply {
            state.observe(this@MviFragment, Observer { viewState -> render(viewState!!) })
            events.observe(this@MviFragment, Observer { event -> onEvent(event!!) })
            bind(createIntents(), null)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getViewModel().apply {
            unbind()
        }
    }

    private fun getViewModel(): VM {
        return myViewModel
    }

}
package d42.cavar.papercut

import io.reactivex.Observable

inline fun <reified D: Any> Observable<in D>.intent(): Observable<D> {
    return compose { upstream -> upstream.ofType(D::class.java) }
}
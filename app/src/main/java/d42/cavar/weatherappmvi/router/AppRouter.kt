package d42.cavar.weatherappmvi.router

import androidx.fragment.app.Fragment
import d42.cavar.app_commons.router.Router
import d42.cavar.app_home.home.ui.HomeFragment
import d42.cavar.app_map.ui.MapFragment
import d42.cavar.app_splash.ui.SplashFragment

class AppRouter : Router {

    override fun createSplashFragment(): Fragment {
        return SplashFragment.newInstance()
    }

    override fun createHomeFragment(): Fragment {
        return HomeFragment.newInstance()
    }

    override fun createMapFragment(): Fragment {
        return MapFragment.newInstance()
    }
}
package d42.cavar.weatherappmvi.di

import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import d42.cavar.api_common.AutoCompleteRestInterface
import d42.cavar.api_common.WeatherRestInterface
import d42.cavar.api_common.interactors.auto_complete.AutoCompleteInteractor
import d42.cavar.api_common.interactors.auto_complete.AutoCompleteInteractorImpl
import d42.cavar.api_common.interactors.location.LocationInteractor
import d42.cavar.api_common.interactors.location.LocationInteractorImpl
import d42.cavar.api_common.interactors.weather.WeatherInteractor
import d42.cavar.api_common.interactors.weather.WeatherInteractorImpl
import d42.cavar.app_api.service.auto_complete.createAutoCompleteWebService
import d42.cavar.app_api.service.weather.*
import d42.cavar.app_commons.cache.Cache
import d42.cavar.app_commons.router.Router
import d42.cavar.app_home.domain.HomeContext
import d42.cavar.app_home.home.ui.HomeViewModel
import d42.cavar.app_home.weather_forecast.domain.WatherForecastContext
import d42.cavar.app_home.weather_forecast.ui.WeatherForecastViewModel
import d42.cavar.app_location_api.service.location.provideLocationService
import d42.cavar.app_location_api.service.location.provideRxLocation
import d42.cavar.app_map.domain.MapContext
import d42.cavar.app_map.ui.MapViewModel
import d42.cavar.app_splash.domain.SplashContext
import d42.cavar.app_splash.ui.SplashViewModel
import d42.cavar.weatherappmvi.router.AppRouter
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.experimental.builder.viewModel
import org.koin.dsl.module.module

val appModule = module {
    single { AppRouter() as Router }
    single { Cache() }
    single {androidContext().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager }
}

val locationService = module {
    single { provideRxLocation(androidApplication()) }
    single { provideLocationService() }
}

val interactorModule = module {
    factory { LocationInteractorImpl(get(), get()) as LocationInteractor }
    factory { WeatherInteractorImpl(get()) as WeatherInteractor }
    factory { AutoCompleteInteractorImpl(get()) as AutoCompleteInteractor }
}

val serviceWeatherApiModule = module {
    single { createWeatherWebService<WeatherRestInterface>() }
}

val serviceAutoCompleteApiModule = module {
    single { createAutoCompleteWebService<AutoCompleteRestInterface>() }
}

val splashModule = module {
    factory { SplashContext(get(), get()) }
    viewModel<SplashViewModel>()
}

val homeModule = module {
    factory { HomeContext(get(), get(), get()) }
    factory { WatherForecastContext(get()) }
    viewModel<HomeViewModel>()
    viewModel<WeatherForecastViewModel>()
}

val mapModule = module {
    factory { MapContext(get(), get()) }
    viewModel<MapViewModel>()
}
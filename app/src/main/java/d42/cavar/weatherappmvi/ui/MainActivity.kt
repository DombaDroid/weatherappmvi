package d42.cavar.weatherappmvi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import d42.cavar.app_commons.router.Router
import d42.cavar.app_splash.ui.SplashFragment
import d42.cavar.weatherappmvi.R
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val router: Router by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportFragmentManager.findFragmentById(android.R.id.content) == null) {
            val fragmeManager = supportFragmentManager
            val fragmentTransaction = fragmeManager?.beginTransaction()
            fragmentTransaction?.add(android.R.id.content, router.createSplashFragment(), SplashFragment.TAG)
            fragmentTransaction?.commit()
        }
    }
}

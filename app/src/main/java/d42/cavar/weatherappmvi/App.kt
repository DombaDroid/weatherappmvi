package d42.cavar.weatherappmvi

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import d42.cavar.weatherappmvi.di.*
import io.reactivex.internal.functions.Functions
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.android.startKoin



class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setErrorHandler(Functions.emptyConsumer())
        startKoin(this, listOf(appModule, serviceWeatherApiModule, serviceAutoCompleteApiModule, locationService, splashModule, homeModule, mapModule, interactorModule))
    }
}